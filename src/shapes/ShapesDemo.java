/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package shapes;

import static org.junit.Assert.assertEquals;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class ShapesDemo {

    public static void main(String[] args) {
        ShapesDemo.calculateArea(new Rectangle());
        ShapesDemo.calculateArea(new Square());
    }

    private static void calculateArea(Rectangle r) {
        r.setWidth(2);
        r.setHeight(3);
        assertEquals("Area calculation is incorrect",6,r.getArea());
    }

}
